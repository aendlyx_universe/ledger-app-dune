#*******************************************************************************
#   Ledger App
#   (c) 2017 Ledger
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.
#*******************************************************************************

# Version
APPVERSION_M=0
APPVERSION_N=4
APPVERSION_P=2
APPVERSION_D=20190910

# Enable PRINTF (with specific runtime on Ledger)
DEBUG = 0
# Enable PRINTF_STACK_SIZE (with modified Bolos)
DEBUG_MEM = 0
# Exception contains line number in parser instead of parse error
DEBUG_PARSER = 0

ifeq ($(BOLOS_SDK),)
$(error Environment variable BOLOS_SDK is not set)
endif
include $(BOLOS_SDK)/Makefile.defines

ifeq ($(TARGET),NANOX)
TARGET_ID=0x33000004
TARGET_NAME=TARGET_NANOX
TARGET_SHORT=nano-x
else
TARGET_SHORT=nano-s
endif

# You can use APP=Tezos to build a Tezos-compatible app

ifeq ($(APP),)
APPNAME = "Dune"
KIND=dune
DEFINES+= DUNE_APP
else
APPNAME = "Tezos"
KIND=tezos
DEFINES+= TEZOS_APP
endif

APP_LOAD_PARAMS=--appFlags 0 --curve ed25519 --curve secp256k1 --curve prime256r1 --path "44'/1729'" $(COMMON_LOAD_PARAMS)

GIT_DESCRIBE ?= $(shell git describe --tags --abbrev=8 --always --long --dirty 2>/dev/null)

APPVERSION=$(APPVERSION_M).$(APPVERSION_N).$(APPVERSION_P)-$(APPVERSION_D)

COMMIT ?= $(shell echo "$(GIT_DESCRIBE)" | awk -F'-g' '{print $$$$2}' | sed 's/-dirty/*/')
ifeq ($(COMMIT),)
  $(warning COMMIT not specified and could not be determined with git from "$(GIT_DESCRIBE)")
else
  $(info COMMIT=$(COMMIT))
endif

ICONNAME=icons/${TARGET_SHORT}-${KIND}.gif

################
# Default rule #
################
all: show-app default

obj/apdu_version.o: Makefile

.PHONY: show-app
show-app:
	@echo ">>>>> Building $(APP) at commit $(COMMIT)"

show-nano:
	@echo TARGET: ID=${TARGET_ID} NAME=${TARGET_NAME}

watch-log:
	usbtool -v 0x2c97 log

############
# Platform #
############

DEFINES   += OS_IO_SEPROXYHAL
DEFINES   += HAVE_BAGL HAVE_SPRINTF
DEFINES   += HAVE_IO_USB HAVE_L4_USBLIB IO_USB_MAX_ENDPOINTS=6 IO_HID_EP_LENGTH=64 HAVE_USB_APDU
DEFINES   += VERSION=\"$(APPVERSION)\" COMMIT=\"$(COMMIT)\"  \
		APPVERSION_M=$(APPVERSION_M) APPVERSION_N=$(APPVERSION_N) \
		APPVERSION_P=$(APPVERSION_P) APPVERSION_D=\"$(APPVERSION_D)\"

# U2F
# Note: using U2F causes 148 bytes on the stack
DEFINES   += HAVE_U2F HAVE_IO_U2F
DEFINES   += U2F_PROXY_MAGIC=\"XTZ\" # Let's remain compatible
DEFINES   += USB_SEGMENT_SIZE=64
DEFINES   += BLE_SEGMENT_SIZE=32 #max MTU, min 20

DEFINES   += HAVE_BOLOS_APP_STACK_CANARY

DEFINES   += UNUSED\(x\)=\(void\)x

ifeq ($(TARGET_NAME),TARGET_NANOX)
DEFINES   += IO_SEPROXYHAL_BUFFER_SIZE_B=300
DEFINES   += HAVE_BLE BLE_COMMAND_TIMEOUT_MS=2000
DEFINES   += HAVE_BLE_APDU # basic ledger apdu transport over BLE

DEFINES   += HAVE_GLO096 HAVE_UX_FLOW
DEFINES   += HAVE_BAGL BAGL_WIDTH=128 BAGL_HEIGHT=64
DEFINES   += HAVE_BAGL_ELLIPSIS # long label truncation feature
DEFINES   += HAVE_BAGL_FONT_OPEN_SANS_REGULAR_11PX
DEFINES   += HAVE_BAGL_FONT_OPEN_SANS_EXTRABOLD_11PX
DEFINES   += HAVE_BAGL_FONT_OPEN_SANS_LIGHT_16PX

SDK_SOURCE_PATH  += lib_blewbxx lib_blewbxx_impl
SDK_SOURCE_PATH  += lib_ux
else
DEFINES   += IO_SEPROXYHAL_BUFFER_SIZE_B=128
endif

ifeq ($(DEBUG_MEM),1)
DEFINES   += HAVE_PATCHED_BOLOS
endif

ifeq ($(DEBUG_PARSER),1)
DEFINES   += DEBUG_PARSER
endif

ifneq ($(DEBUG),0)

        ifeq ($(TARGET_NAME),TARGET_NANOX)
                DEFINES   += HAVE_PRINTF PRINTF=mcu_usb_printf
        else
                DEFINES   += HAVE_PRINTF PRINTF=screen_printf
        endif
	DEFINES += DUNE_DEBUG
else
        DEFINES   += PRINTF\(...\)=
endif

.PHONY: distrib

ARCHIVE:=ledger-app-dune-${TARGET_SHORT}-bin-${APPVERSION}

distrib:
	cat debug/app.map |grep _nvram_data_size | tr -s ' ' | cut -f2 -d' ' > distrib/nvram_size.txt
	python ${BOLOS_SDK}/icon.py ${ICONNAME} hexbitmaponly > distrib/icon.hex 
	cp bin/app.hex distrib/
	echo ${COMMIT} > distrib/VERSION
	cp -dpR distrib ${ARCHIVE}
	tar zcf ${ARCHIVE}.tar.gz ${ARCHIVE}
	rm -rf ${ARCHIVE}

push: distrib
	scp ${ARCHIVE}.tar.gz dune.network:/home/www.dune.network/www/files/ledger-app-dune/${TARGET_SHORT}/

##############
# Compiler #
##############
ifneq ($(BOLOS_ENV),)
$(info BOLOS_ENV=$(BOLOS_ENV))
CLANGPATH := $(BOLOS_ENV)/clang-arm-fropi/bin/
GCCPATH := $(BOLOS_ENV)/gcc-arm-none-eabi-5_3-2016q1/bin/
else
$(info BOLOS_ENV is not set: falling back to CLANGPATH and GCCPATH)
endif
ifeq ($(CLANGPATH),)
$(info CLANGPATH is not set: clang will be used from PATH)
endif
ifeq ($(GCCPATH),)
$(info GCCPATH is not set: arm-none-eabi-* will be used from PATH)
endif

CC       := $(CLANGPATH)clang

CFLAGS   += -O3 -Os -Wall -Wextra 

AS     := $(GCCPATH)arm-none-eabi-gcc

LD       := $(GCCPATH)arm-none-eabi-gcc
LDFLAGS  += -O3 -Os
LDLIBS   += -lm -lgcc -lc

# import rules to compile glyphs(/pone)
include $(BOLOS_SDK)/Makefile.glyphs

### computed variables
APP_SOURCE_PATH  += src
SDK_SOURCE_PATH  += lib_stusb lib_stusb_impl lib_u2f

load: all
	python -m ledgerblue.loadApp $(APP_LOAD_PARAMS)

delete:
	python -m ledgerblue.deleteApp $(COMMON_DELETE_PARAMS)

# import generic rules from the sdk
include $(BOLOS_SDK)/Makefile.rules

#add dependency on custom makefile filename
dep/%.d: %.c Makefile

listvariants:
	@echo VARIANTS APP tezos_wallet tezos_baking
