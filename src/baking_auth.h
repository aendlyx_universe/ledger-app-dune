#pragma once

#include "apdu.h"
#include "parse_operations.h"
#include "protocol.h"
#include "types.h"

#include <stdbool.h>
#include <stdint.h>

void authorize_baking(cx_curve_t const curve,
                      bip32_path_t const *const bip32_path);
void guard_baking_authorized(parsed_baking_data_t *const baking_data,
                             bip32_path_with_curve_t const *const key);
bool is_valid_level(level_t level);
void write_high_water_mark(parsed_baking_data_t const *const in);

// Return false if it is invalid
bool parse_baking_data(parsed_baking_data_t *const out, void const *const data, size_t const length);
