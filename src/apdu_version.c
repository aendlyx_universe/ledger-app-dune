#include "apdu.h"
#include "globals.h"
#include "to_string.h"
//#include "apdu_version.h"

#include <stdbool.h>
#include <stdint.h>
#include <string.h>

typedef struct version {
    uint8_t class;
    uint8_t major;
    uint8_t minor;
    uint8_t patch;
    char date[8];
} version_t;

const version_t version = { 0,
                            APPVERSION_M,
                            APPVERSION_N,
                            APPVERSION_P,
                            APPVERSION_D };

static const char commit[] = COMMIT;

size_t handle_apdu_version(uint8_t __attribute__((unused)) instruction) {
  version_t v = version ;
  v.class = global.baking_auth.new_data.baking_mode ;
  memcpy(G_io_apdu_buffer, &v, sizeof(version_t));
  size_t tx = sizeof(version_t);
  memcpy(G_io_apdu_buffer+tx, commit, sizeof(commit));
  tx += sizeof(commit);
  return finalize_successful_send(tx);
}

size_t handle_apdu_git(uint8_t __attribute__((unused)) instruction) {
  memcpy(G_io_apdu_buffer, commit, sizeof(commit));
  size_t tx = sizeof(commit);
  return finalize_successful_send(tx);
}
