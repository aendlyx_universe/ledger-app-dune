#pragma once

#include "exception.h"
#include "os.h"
#include "os_io_seproxyhal.h"

#include <stdbool.h>
#include <string.h>

// Baking Auth
#define MAX_BIP32_PATH 5
#define MAX_SETUP_KEYS 3

// Memory:
// With MAX_BIP32_PATH=10 and MAX_SETUP_KEYS=3, the stack size in main_loop is
// 1160, not enough to run
// With MAX_BIP32_PATH=5 and MAX_SETUP_KEYS=3, the stack size in main_loop is
// 1240, enough to run !


#define COMPARE(a, b) ({                                                \
      _Static_assert(sizeof(*a) == sizeof(*b), "Size mismatch in COMPARE"); \
      check_null(a);                                                    \
      check_null(b);                                                    \
      memcmp(a, b, sizeof(*a));                                         \
    })
#define NUM_ELEMENTS(a) (sizeof(a)/sizeof(*a))


// Macro that produces a compile-error showing the sizeof the argument.
#define ERROR_WITH_NUMBER_EXPANSION(x) char (*__kaboom)[x] = 1;
#define SIZEOF_ERROR(x)  ERROR_WITH_NUMBER_EXPANSION(sizeof(x));

// Type-safe versions of true/false
#undef true
#define true ((bool)1)
#undef false
#define false ((bool)0)

// Return number of bytes to transmit (tx)
typedef size_t (*apdu_handler)(uint8_t instruction);

typedef uint32_t level_t;

#ifdef TEZOS_APP
#define CHAIN_ID_BASE58_STRING_SIZE sizeof("NetXdQprcVkpaWU")
#else
#define CHAIN_ID_BASE58_STRING_SIZE sizeof("NetXwhYbWGa82xo")
#endif

#define MAX_INT_DIGITS 20

typedef struct {
  size_t length;
  size_t size;
  uint8_t *bytes;
} buffer_t;

typedef struct {
  uint32_t v;
} chain_id_t;

// curl http://mainnet-node.dunscan.io/chains/main/chain_id
// dune-client dune decode b58check NetXwhYbWGa82xo
#if TEZOS_APP
// Mainnet Chain ID: NetXdQprcVkpaWU (Tezos)
static chain_id_t const mainnet_chain_id = { .v = 0x7A06A770 };
#else
// Mainnet Chain ID: NetXwhYbWGa82xo (Dune)
static chain_id_t const mainnet_chain_id = { .v = 0xe75d4a33 };
#endif

#if defined(HAVE_BOLOS_APP_STACK_CANARY) && defined(HAVE_PATCHED_BOLOS)
extern void bolos_check_canary();
extern int bolos_stack_size();
extern int bolos_canary_diff();
#define PRINTF_STACK_SIZE(y) \
  PRINTF("%d freestack %s: %d\n", bolos_canary_diff(), y, bolos_stack_size())
#else
#define PRINTF_STACK_SIZE(y)
#endif

// UI
typedef bool (*ui_callback_t)(void); // return true to go back to idle screen

// Uses K&R style declaration to avoid being stuck on const void *, to avoid having to cast the
// function pointers.
typedef void (*string_generation_callback)(/* char *buffer, size_t buffer_size, const void *data */);

// Keys
struct key_pair {
  cx_ecfp_public_key_t public_key;
  cx_ecfp_private_key_t private_key;
};

typedef struct {
  uint8_t length;
  uint32_t components[MAX_BIP32_PATH];
} bip32_path_t;

static inline void copy_bip32_path(bip32_path_t *const out, bip32_path_t const *const in) {
  check_null(out);
  check_null(in);
  memcpy(out->components, in->components, in->length * sizeof(*in->components));
  out->length = in->length;
}

static inline bool bip32_paths_eq(bip32_path_t const *const a, bip32_path_t const *const b) {
  return a == b || (
                    a != NULL &&
                    b != NULL &&
                    a->length == b->length &&
                    memcmp(a->components, b->components, a->length * sizeof(*a->components)) == 0
                    );
}


typedef struct {
  bip32_path_t bip32_path;
  cx_curve_t curve;
} bip32_path_with_curve_t;

static inline void copy_bip32_path_with_curve(bip32_path_with_curve_t *const out, bip32_path_with_curve_t const *const in) {
  check_null(out);
  check_null(in);
  copy_bip32_path(&out->bip32_path, &in->bip32_path);
  out->curve = in->curve;
}

static inline bool bip32_path_with_curve_eq(bip32_path_with_curve_t const *const a, bip32_path_with_curve_t const *const b) {
  return a == b || (
                    a != NULL &&
                    b != NULL &&
                    bip32_paths_eq(&a->bip32_path, &b->bip32_path) &&
                    a->curve == b->curve
                    );
}


typedef struct {
  level_t highest_level;
  bool had_endorsement;
} high_watermark_t;

typedef struct {
  high_watermark_t main;
  high_watermark_t test;
} hwm;

typedef struct {
  chain_id_t main_chain_id;
  hwm hwm;
  bip32_path_with_curve_t baking_key;
} baker;

typedef struct {
  baker bakers[MAX_SETUP_KEYS];
  uint8_t nbakers;
  uint8_t baking_mode; // 0=wallet, 1=baking-mode, 2=baking-mode-no-switch-back
} nvram_data;

#define SIGN_HASH_SIZE 32 // TODO: Rename or use a different constant.

#define PKH_STRING_SIZE 40 // includes null byte // TODO: use sizeof for this.
#define PROTOCOL_HASH_BASE58_STRING_SIZE sizeof("ProtoBetaBetaBetaBetaBetaBetaBetaBetaBet11111a5ug96")
#define CODE_HASH_BASE58_STRING_SIZE sizeof("expru2bNJAfCERVXp1Wgq9AgnFEew83ehHxbXKxRQNYfEX6pa2LYkJ")

#define MAX_SCREEN_COUNT 8 // Current maximum usage
#define PROMPT_WIDTH 16
#define VALUE_WIDTH CODE_HASH_BASE58_STRING_SIZE

// Macros to wrap a static prompt and value strings and ensure they aren't too long.
#define PROMPT(str) ({                                                  \
      _Static_assert(sizeof(str) <= PROMPT_WIDTH + 1/*null byte*/ , str " won't fit in the UI prompt."); \
      str;                                                              \
    })

#define STATIC_UI_VALUE(str) ({                                         \
      _Static_assert(sizeof(str) <= VALUE_WIDTH + 1/*null byte*/, str " won't fit in the UI.".); \
      str;                                                              \
    })


// Operations
#define PROTOCOL_HASH_SIZE 32
#define CODE_HASH_SIZE 32

// TODO: Rename to KEY_HASH_SIZE
#define HASH_SIZE 20

#define BAKING_KIND_BLOCK       0
#define BAKING_KIND_ENDORSEMENT 1
#define BAKING_KIND_TEST        2

typedef struct {
  chain_id_t chain_id;
  int baking_kind;
  level_t level;
  int baker;
} parsed_baking_data_t;

struct parsed_contract {
  uint8_t originated; // a lightweight bool
  uint8_t curve_code; // DUNE_NO_CURVE in originated case
  // An implicit contract with DUNE_NO_CURVE means not present
  uint8_t hash[HASH_SIZE];
};

struct parsed_proposal {
  uint32_t voting_period;
  // TODO: Make 32 bit version of number_to_string_indirect
  uint8_t protocol_hash[PROTOCOL_HASH_SIZE];
};

enum ballot_vote {
  BALLOT_VOTE_YEA,
  BALLOT_VOTE_NAY,
  BALLOT_VOTE_PASS,
};

struct parsed_ballot {
  uint32_t voting_period;
  uint8_t protocol_hash[PROTOCOL_HASH_SIZE];
  enum ballot_vote vote;
};

enum operation_tag {
  OPERATION_TAG_NONE = -1, // Sentinal value, as 0 is possibly used for something
  OPERATION_TAG_PROPOSAL = 5,
  OPERATION_TAG_BALLOT = 6,
  OPERATION_TAG_REVEAL = 7,
  OPERATION_TAG_TRANSACTION = 8,
  OPERATION_TAG_ORIGINATION = 9,
  OPERATION_TAG_DELEGATION = 10,
  OPERATION_TAG_COLLECT_CALL = 200,
  OPERATION_TAG_DUNE_MANAGER = 201,
};

enum dune_operation_tag {
  DUNE_OPERATION_TAG_ACTIVATE = 0,
  DUNE_OPERATION_TAG_MANAGE_ACCOUNTS = 1,
};

// TODO: Make this an enum.
// Flags for parsed_operation.flag
#define ORIGINATION_FLAG_SPENDABLE 0b001
#define ORIGINATION_FLAG_DELEGATABLE 0b010
#define ORIGINATION_FLAG_HAS_SCRIPT 0b100
#define ORIGINATION_FLAG_HAS_SCRIPT_HASH 0b1000

#define TRANSACTION_FLAG_COLLECT_CALL 0b001
#define TRANSACTION_FLAG_HAS_PARAM 0b100

#define DUNE_MANAGE_ACCOUNTS_FLAG 0b01
#define DUNE_ACTIVATE_FLAG_HAS_PROTO 0b010
#define DUNE_ACTIVATE_FLAG_HAS_PARAM 0b100


struct parsed_operation {
  enum operation_tag tag;
  struct parsed_contract source;
  struct parsed_contract destination;
  struct parsed_contract delegate; // For originations only
  struct parsed_proposal proposal; // For proposals only
  struct parsed_ballot ballot; // For ballots only
  uint64_t amount; // 0 where inappropriate
  uint32_t flags;  // Interpretation depends on operation type
};

struct parsed_operation_group {
  cx_ecfp_public_key_t public_key; // compressed
  uint64_t total_fee;
  uint64_t total_storage_limit;
  bool has_reveal;
  struct parsed_contract signing;
  struct parsed_operation operation;
};

enum validation_state {
  UNKNOWN_VSTATE = 0,
  INVALID_VSTATE = -1,
  VALID_VSTATE = 1,
};


#define STRCPY(buff, x) ({                                              \
      _Static_assert(sizeof(buff) >= sizeof(x) && sizeof(*x) == sizeof(char), "String won't fit in buffer"); \
      strcpy(buff, x);                                                  \
    })

#undef MAX
#define MAX(a, b) ({                            \
      __typeof__(a) ____a_ = (a);               \
      __typeof__(b) ____b_ = (b);               \
      ____a_ > ____b_ ? ____a_ : ____b_;        \
    })

#undef MIN
#define MIN(a, b) ({                            \
      __typeof__(a) ____a_ = (a);               \
      __typeof__(b) ____b_ = (b);               \
      ____a_ < ____b_ ? ____a_ : ____b_;        \
    })
