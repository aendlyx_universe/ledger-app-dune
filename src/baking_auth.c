
#include "baking_auth.h"

#include "apdu.h"
#include "globals.h"
#include "keys.h"
#include "types.h"
#include "protocol.h"
#include "to_string.h"
#include "ui_nano_s.h"

#include "os_cx.h"

#include <string.h>

bool is_valid_level(level_t lvl) {
  return !(lvl & 0xC0000000);
}

void write_high_water_mark(parsed_baking_data_t const *const in) {
  check_null(in);
  if( in->baking_kind == BAKING_KIND_TEST ) return;
  if( in->baker < 0 || !is_valid_level(in->level)) THROW(EXC_WRONG_VALUES);

  if( RAM_WATERMARK() ){
    high_watermark_t *const dest =
      select_hwm_by_chain(in->chain_id,
                          &CACHE_NVRAM.bakers[in->baker]);
    dest->highest_level = MAX(in->level, dest->highest_level);
    dest->had_endorsement = ( in->baking_kind == BAKING_KIND_ENDORSEMENT );
    update_baking_idle_screens();     
  } else {
    UPDATE_NVRAM(ram, {
        // If the chain matches the main chain *or* the main chain is
        // not set, then use 'main' HWM. select_hwm_by_chain is
        // defined in globals.c
        high_watermark_t *const dest =
          select_hwm_by_chain(in->chain_id,
                              &ram->bakers[in->baker]);
        dest->highest_level = MAX(in->level, dest->highest_level);
        dest->had_endorsement = ( in->baking_kind == BAKING_KIND_ENDORSEMENT );
      });
  }
}

/* Set the first baker key only */
void authorize_baking(cx_curve_t const curve,
                      bip32_path_t const *const bip32_path) {
  check_null(bip32_path);
  if (bip32_path->length >
      NUM_ELEMENTS(CACHE_NVRAM.bakers[0].baking_key.bip32_path.components) ||
      bip32_path->length == 0) return;

  UPDATE_NVRAM(ram, {
      if( ram->nbakers == 0 ) ram->nbakers = 1;
      ram->bakers[0].baking_key.curve = curve;
      copy_bip32_path(&ram->bakers[0].baking_key.bip32_path, bip32_path);
    });
}

static bool is_level_authorized(parsed_baking_data_t const *const baking_info) {
  check_null(baking_info);
  if (baking_info->baking_kind == BAKING_KIND_TEST) return true;
  if (!is_valid_level(baking_info->level)) return false;
  high_watermark_t const *const hwm =
    select_hwm_by_chain(baking_info->chain_id,
                        &CACHE_NVRAM.bakers[baking_info->baker]);
  return baking_info->level > hwm->highest_level

    // Levels are tied. In order for this to be OK, this must be an
    // endorsement, and we must not have previously seen an
    // endorsement.
    || (baking_info->level == hwm->highest_level
        && baking_info->baking_kind == BAKING_KIND_ENDORSEMENT
        && !hwm->had_endorsement);
}

static int is_path_authorized(cx_curve_t curve,
                              bip32_path_t const *const bip32_path) {
  check_null(bip32_path);
  for(int i = 0; i < CACHE_NVRAM.nbakers ; i++){
    if(
       curve == CACHE_NVRAM.bakers[i].baking_key.curve &&
       bip32_path->length > 0 &&
       bip32_paths_eq(bip32_path, &CACHE_NVRAM.bakers[i].baking_key.bip32_path)

       ) return i;
  }
  return -1;
}

void guard_baking_authorized(parsed_baking_data_t *const baking_info,
                             bip32_path_with_curve_t const *const key) {
  check_null(baking_info);
  check_null(key);
  baking_info->baker = is_path_authorized(key->curve, &key->bip32_path);
  if( baking_info->baker < 0 ) THROW(EXC_SECURITY);
  if (!is_level_authorized(baking_info)) THROW(EXC_WRONG_VALUES);
}
