#include "apdu_pubkey.h"

#include "apdu.h"
#include "cx.h"
#include "globals.h"
#include "keys.h"
#include "protocol.h"
#include "to_string.h"
#include "ui_nano_s.h"
#include "baking_auth.h"

#include <string.h>

#define G global.u.pubkey

size_t provide_pubkey(uint8_t *const io_buffer,
                      cx_ecfp_public_key_t const *const pubkey) {
  size_t tx = 0;
  io_buffer[tx++] = pubkey->W_len;
  memmove(io_buffer + tx, pubkey->W, pubkey->W_len);
  tx += pubkey->W_len;
  return finalize_successful_send(tx);
}

static bool pubkey_ok(void) {
  delayed_send(provide_pubkey(G_io_apdu_buffer, &G.public_key));
  return true;
}

static bool baking_ok(void) {
  authorize_baking(G.key.curve, &G.key.bip32_path);
  pubkey_ok();
  return true;
}

char const *const *get_baking_prompts() {
  static const char *const baking_prompts[] = {
    PROMPT("Authorize Baking"),
    PROMPT("Public Key Hash"),
    NULL,
  };
  return baking_prompts;
}

__attribute__((noreturn))
static void prompt_address(
                           bool baking,
                           ui_callback_t ok_cb,
                           ui_callback_t cxl_cb
                           ) {
  static size_t const TYPE_INDEX = 0;
  static size_t const ADDRESS_INDEX = 1;

  if( IS_BAKING_MODE() && baking) {
    REGISTER_STATIC_UI_VALUE(TYPE_INDEX, "With Public Key?");
    register_ui_callback(ADDRESS_INDEX, bip32_path_with_curve_to_pkh_string,
                         &G.key);
    ui_prompt(get_baking_prompts(), ok_cb, cxl_cb);
  } else {
    static const char *const pubkey_labels[] = {
      PROMPT("Verify"),
      PROMPT("Public Key Hash"),
      NULL,
    };
    REGISTER_STATIC_UI_VALUE(TYPE_INDEX, "Public Key Hash");
    register_ui_callback(ADDRESS_INDEX, bip32_path_with_curve_to_pkh_string,
                         &G.key);
    ui_prompt(pubkey_labels, ok_cb, cxl_cb);
  }
}

size_t handle_apdu_get_public_key(uint8_t instruction) {
  if( instruction == INS_AUTHORIZE_BAKING ){
    if ( !IS_BAKING_MODE() ) handle_apdu_error(instruction);
    if ( CACHE_NVRAM.nbakers > 1 ) THROW( EXC_REJECT );
  }
  uint8_t *dataBuffer = G_io_apdu_buffer + OFFSET_CDATA;

  if (READ_UNALIGNED_BIG_ENDIAN(uint8_t, &G_io_apdu_buffer[OFFSET_P1]) != 0)
    THROW(EXC_WRONG_PARAM);

  // do not expose pks without prompt over U2F (browser support)
  if (instruction == INS_GET_PUBLIC_KEY) require_hid();

  uint8_t const curve_code =
    READ_UNALIGNED_BIG_ENDIAN(uint8_t, &G_io_apdu_buffer[OFFSET_CURVE]);
  G.key.curve = curve_code_to_curve(curve_code);

  size_t const cdata_size =
    READ_UNALIGNED_BIG_ENDIAN(uint8_t,&G_io_apdu_buffer[OFFSET_LC]);

  if (IS_BAKING_MODE() &&
      cdata_size == 0 && instruction == INS_AUTHORIZE_BAKING) {
    // not specifying the key does not make sense without prior initialization !
    THROW(EXC_WRONG_PARAM);
    // copy_bip32_path_with_curve(&G.key, &CACHE_NVRAM.bakers[0].baking_key);
  } else {
    read_bip32_path(&G.key.bip32_path, dataBuffer, cdata_size);
    if (IS_BAKING_MODE() &&
        G.key.bip32_path.length == 0) THROW(EXC_WRONG_LENGTH_FOR_INS);
  }
  generate_public_key(&G.public_key, G.key.curve, &G.key.bip32_path);

  if (instruction == INS_GET_PUBLIC_KEY) {
    return provide_pubkey(G_io_apdu_buffer, &G.public_key);
  } else {
    // instruction == INS_PROMPT_PUBLIC_KEY
    // || instruction == INS_AUTHORIZE_BAKING
    ui_callback_t cb;
    bool bake;
    if (IS_BAKING_MODE() &&
        instruction == INS_AUTHORIZE_BAKING) {
      cb = baking_ok;
      bake = true;
    } else {
      // INS_PROMPT_PUBLIC_KEY
      cb = pubkey_ok;
      bake = false;
    }
    prompt_address(bake, cb, delay_reject);
  }
}
