#include "globals.h"

#include "exception.h"
#include "to_string.h"

#ifdef TARGET_NANOX
#include "ux.h"
#endif

#include <string.h>


// WARNING: ***************************************************
// Non-const globals MUST NOT HAVE AN INITIALIZER.
//
// Providing an initializer will cause the application to crash
// if you write to it.
// ************************************************************


globals_t global;

// These are strange variables that the SDK relies on us to define but
// uses directly itself.
#ifdef TARGET_NANOX
ux_state_t G_ux;
bolos_ux_params_t G_ux_params;
#else
ux_state_t ux;
#endif

unsigned char G_io_seproxyhal_spi_buffer[IO_SEPROXYHAL_BUFFER_SIZE_B];

// DO NOT TRY TO INIT THIS. This can only be written via an system call.
// The "N_" is *significant*. It tells the linker to put this in NVRAM.
#    ifdef TARGET_NANOX
nvram_data const N_data_real;
#    else
nvram_data N_data_real;
#    endif

high_watermark_t *select_hwm_by_chain(chain_id_t const chain_id,
                                      baker *const ram) {
  check_null(ram);
  return chain_id.v == ram->main_chain_id.v || ram->main_chain_id.v == 0
    ? &ram->hwm.main
    : &ram->hwm.test;
}

void update_baking_idle_screens(void) {

  if( CACHE_NVRAM.nbakers == 0 ){

    number_to_string(global.ui.baking_idle_screens.hwm, 0);
    STRCPY(global.ui.baking_idle_screens.pkh, "No Key Authorized");
    STRCPY(global.ui.baking_idle_screens.chain, "No Chain Id");


  } else {

    baker *baker = &CACHE_NVRAM.bakers[0];
    
    number_to_string(global.ui.baking_idle_screens.hwm,
                     baker->hwm.main.highest_level);

    cx_ecfp_public_key_t const *const pubkey =
      generate_public_key_return_global
      (baker->baking_key.curve, &baker->baking_key.bip32_path);
    pubkey_to_pkh_string
      (global.ui.baking_idle_screens.pkh,
       sizeof(global.ui.baking_idle_screens.pkh),
       baker->baking_key.curve, pubkey);
    
    chain_id_to_string_with_aliases(global.ui.baking_idle_screens.chain,
                                    sizeof(global.ui.baking_idle_screens.chain),
                                    &baker->main_chain_id);
  }
}
