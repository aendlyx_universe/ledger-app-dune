#include "apdu_baking.h"
#include "apdu_hmac.h"
#include "apdu_pubkey.h"
#include "apdu_setup.h"
#include "apdu_sign.h"
#include "apdu.h"
#include "globals.h"
#include "types.h"
#include "apdu_version.h"


__attribute__((noreturn))
bool exit_app(void) {

  if( IS_BAKING_MODE() ){
    if( RAM_WATERMARK() ){
      SAVE_TO_NVRAM();
    }
    require_pin();
  }

  BEGIN_TRY_L(exit) {
    TRY_L(exit) {
      os_sched_exit(-1);
    }
    FINALLY_L(exit) {
    }
  }
  END_TRY_L(exit);
  
  THROW(0); // Suppress warning
}




__attribute__((noreturn))
void main_loop(void) {
    volatile size_t rx = io_exchange(CHANNEL_APDU, 0);
    while (true) {
      PRINTF_STACK_SIZE("main_loop");
        BEGIN_TRY {
            TRY {
                // Process APDU of size rx
                if (rx == 0) {
                    // no apdu received, well, reset the session, and reset the
                    // bootloader configuration
                    THROW(EXC_SECURITY);
                }

                if (G_io_apdu_buffer[OFFSET_CLA] != CLA) {
                    THROW(EXC_CLASS);
                }

                // The amount of bytes we get in our APDU must match what the APDU declares
                // its own content length is. All these values are unsigned, so this implies
                // that if rx < OFFSET_CDATA it also throws.
                if (rx != G_io_apdu_buffer[OFFSET_LC] + OFFSET_CDATA) {
                    THROW(EXC_WRONG_LENGTH);
                }

                uint8_t const instruction = G_io_apdu_buffer[OFFSET_INS];
                PRINTF("  instruction=%d\n", instruction);
        size_t tx = 0;
        switch(instruction){
        case INS_VERSION :
          tx = handle_apdu_version(instruction); break;
        case INS_GET_PUBLIC_KEY :
          tx = handle_apdu_get_public_key(instruction); break;
        case INS_PROMPT_PUBLIC_KEY :
          tx = handle_apdu_get_public_key(instruction); break;
        case INS_SIGN :
          tx = handle_apdu_sign(true, true, instruction); break;
        case INS_GIT :
          tx = handle_apdu_git(instruction); break;
        case INS_SIGN_WITH_HASH :
          tx = handle_apdu_sign(true, true, instruction); break;
        case INS_AUTHORIZE_BAKING :
          tx = handle_apdu_get_public_key(instruction); break;
        case INS_RESET :
          tx = handle_apdu_reset(instruction); break;
        case INS_QUERY_AUTH_KEY :
          tx = handle_apdu_query_auth_key(instruction); break;
        case INS_QUERY_MAIN_HWM :
          tx = handle_apdu_main_hwm(instruction); break;
        case INS_SETUP :
          tx = handle_apdu_setup(instruction); break;
        case INS_QUERY_ALL_HWM :
          tx = handle_apdu_all_hwm(instruction); break;
        case INS_DEAUTHORIZE :
          tx = handle_apdu_deauthorize(instruction); break;
        case INS_QUERY_AUTH_KEY_WITH_CURVE :
          tx = handle_apdu_query_auth_key_with_curve(instruction); break;
        case INS_HMAC :
          tx = handle_apdu_hmac(instruction); break;
        case INS_SIGN_UNSAFE :
          tx = handle_apdu_sign(false, false, instruction); break;
        case INS_SET_BAKING_MODE :
          tx = handle_set_baking_mode(instruction); break;
        case INS_SET_BAKING_ALWAYS :
          tx = handle_set_baking_always(instruction); break;
        case INS_SETUP_MORE :
          tx = handle_apdu_setup(instruction); break;
        default: tx = handle_apdu_error (instruction);
        }
 

                
                rx = io_exchange(CHANNEL_APDU, tx);
            }
            CATCH(ASYNC_EXCEPTION) {
                rx = io_exchange(CHANNEL_APDU | IO_ASYNCH_REPLY, 0);
            }
            CATCH(EXCEPTION_IO_RESET) {
                THROW(EXCEPTION_IO_RESET);
            }
            CATCH_OTHER(e) {
                uint16_t sw = e;
                switch (sw) {
                default:
                    sw = 0x6800 | (e & 0x7FF);
                    // FALL THROUGH
                case 0x6000 ... 0x6FFF:
                case 0x9000 ... 0x9FFF: {
                        size_t tx = 0;
                        G_io_apdu_buffer[tx++] = sw >> 8;
                        G_io_apdu_buffer[tx++] = sw;
                        rx = io_exchange(CHANNEL_APDU, tx);
                        break;
                    }
                }
            }
            FINALLY {
            }
        }
        END_TRY;
    }
}


size_t handle_apdu_error(uint8_t __attribute__((unused)) instruction) {
    THROW(EXC_INVALID_INS);
}


void init_globals(void) {
    memset(&global, 0, sizeof(global));
    
    /* Load the NVRAM to know in which mode we are */
    LOAD_FROM_NVRAM();

#ifdef TARGET_NANOX
    memset(&G_ux, 0, sizeof(G_ux));
    memset(&G_ux_params, 0, sizeof(G_ux_params));
#else
    memset(&ux, 0, sizeof(ux));
#endif

    memset(G_io_seproxyhal_spi_buffer, 0, sizeof(G_io_seproxyhal_spi_buffer));
}



__attribute__((section(".boot"))) int main(void) {
    // exit critical section
    __asm volatile("cpsie i");

    // ensure exception will work as planned
    os_boot();  // SDK

    uint8_t tag;
    init_globals();
    global.stack_root = &tag;

    for (;;) {
        BEGIN_TRY {
            TRY {
                UX_INIT();

                io_seproxyhal_init(); // SDK

                USB_power(0);
                USB_power(1);

                ui_initial_screen();

                main_loop();
            }
            CATCH(EXCEPTION_IO_RESET) {
                // reset IO and UX
                continue;
            }
            CATCH_OTHER(e) {
                break;
            }
            FINALLY {
            }
        }
        END_TRY;
    }

    // Only reached in case of uncaught exception
    if( IS_BAKING_MODE() ){
      io_seproxyhal_power_off(); // Should not be allowed dashboard access
    } else {
      exit_app();
    }
    return 0;
}




#undef MIN
#define MIN(a,b) (unsigned int)(a) < (unsigned int)(b) ? a : b

unsigned char io_event(__attribute__((unused)) unsigned char channel) {
    // nothing done with the event, throw an error on the transport layer if
    // needed

    // can't have more than one tag in the reply, not supported yet.
    switch (G_io_seproxyhal_spi_buffer[0]) {
    case SEPROXYHAL_TAG_FINGER_EVENT:
        UX_FINGER_EVENT(G_io_seproxyhal_spi_buffer);
        break;

    case SEPROXYHAL_TAG_BUTTON_PUSH_EVENT:
        UX_BUTTON_PUSH_EVENT(G_io_seproxyhal_spi_buffer);
        break;

    case SEPROXYHAL_TAG_STATUS_EVENT:
      if (G_io_apdu_media == IO_APDU_MEDIA_USB_HID && !(U4BE(G_io_seproxyhal_spi_buffer, 3) & SEPROXYHAL_TAG_STATUS_EVENT_FLAG_USB_POWERED)) {
        THROW(EXCEPTION_IO_RESET);
      }
      // no break is intentional
    default:
      UX_DEFAULT_EVENT();
      break;
      
    case SEPROXYHAL_TAG_DISPLAY_PROCESSED_EVENT:
        UX_DISPLAYED_EVENT({});
        break;

    case SEPROXYHAL_TAG_TICKER_EVENT:
      
      /* Do not use UX_TICKER_EVENT, because it may set the ledger back
         in auto-lock mode. We don't want that in Baking mode, we always
         need to be ready to bake/endorse! */
      if( IS_BAKING_MODE() ){
        if (ux.callback_interval_ms != 0) {
          ux.callback_interval_ms -= MIN(ux.callback_interval_ms, 100u);
          if (ux.callback_interval_ms == 0) {
            ui_ticker();
          }
        }
      } else {
        UX_TICKER_EVENT(G_io_seproxyhal_spi_buffer,
                        {
                          if (UX_ALLOWED) {
                            // prepare next screen
                            ui_ticker();
                          }
                        });
      }
      break;
    }

    // close the event if not done previously (by a display or whatever)
    if (!io_seproxyhal_spi_is_status_sent()) {
        io_seproxyhal_general_status();
    }
    // command has been processed, DO NOT reset the current APDU transport
    // TODO: I don't understand that comment or what this return value means
    return 1;
}




/********************************************************************/
/*                                                                  */
/*          SDK Boilerplate                                         */
/*                                                                  */
/********************************************************************/

void io_seproxyhal_display(const bagl_element_t *element) {
  return io_seproxyhal_display_default((bagl_element_t *)element);
}

unsigned short io_exchange_al(unsigned char channel, unsigned short tx_len) {
  switch (channel & ~(IO_FLAGS)) {
  case CHANNEL_KEYBOARD:
    break;
    
    // multiplexed io exchange over a SPI channel and TLV encapsulated protocol
        case CHANNEL_SPI:
          if (tx_len) {
            io_seproxyhal_spi_send(G_io_apdu_buffer, tx_len);
            
            if (channel & IO_RESET_AFTER_REPLIED) {
              reset();
            }
            return 0; // nothing received from the master so far (it's a tx
            // transaction)
          } else {
            return io_seproxyhal_spi_recv(G_io_apdu_buffer,
                                          sizeof(G_io_apdu_buffer), 0);
          }
          
        default:
          THROW(INVALID_PARAMETER);
  }
  return 0;
}
